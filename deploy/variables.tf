variable "prefix" {
  type        = string
  default     = "raad"
  description = "Receipe name of DevOps course"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "psingh79@gmail.com"
}